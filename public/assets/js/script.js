// fizz buzz

function getFizzBuzz(num){
	if (num % 3 == 0 && num % 5 == 0){
		return "FizzBuzz";
	} else if (num % 3 == 0) {
		return "Fizz";
	} else if (num % 5 == 0) {
		return "Buzz";
	} else {
		return "Pop";
	}
}

console.log(getFizzBuzz(5));
console.log(getFizzBuzz(30));
console.log(getFizzBuzz(27));
console.log(getFizzBuzz(17));

//2 roygbiv

function roygbiv(letter){
	let letterCleaned = letter.toLowerCase();
	switch(letterCleaned){
		case "r":
			return "Red";
			break;
		case "o":
			return "Orange";
			break;
		case "y":
			return "Yellow";
			break;
		case "g":
			return "Green";
			break;
		case "b":
			return "Blue";
			break;
		case "i":
			return "Indigo";
			break;
		case "v":
			return "Violet";
			break;
		default:
			return "No color";
	}
}

console.log(roygbiv("r"));
console.log(roygbiv("G"));
console.log(roygbiv("x"));


// 3 leap year
function leapYear(year){
	if(year % 4 == 0){
		if (year % 100 == 0){
			if (year % 400 == 0){
				return "A leap year";
			} else {
				return "Not a leap year";
			}
		} else {
			return "A leap year";
		}
	} else {
		return "Not a leap year";	
	}
}
